const loding = document.getElementsByTagName('p')[0],
      wendu = document.getElementsByClassName('li1')[0],
      tianqi = document.getElementsByClassName('li2')[0], 
      yaqiang = document.getElementsByClassName('li3')[0],
      yunliang = document.getElementsByClassName('li4')[0],
      xiangduiwen = document.getElementsByClassName('li5')[0],
      fengxiang = document.getElementsByClassName('li6')[0],
      guance = document.getElementsByClassName('li7')[0],
      fengsu = document.getElementsByClassName('li8')[0],
      fengli = document.getElementsByClassName('li9')[0],
      nengjian = document.getElementsByClassName('li10')[0],
      jiangshui = document.getElementsByClassName('li11')[0],
      buttonleft = document.getElementsByClassName('left')[0],
      buttonright = document.getElementsByClassName('right')[0],
      left = document.getElementsByClassName('first')[0],
      right = document.getElementsByClassName('double')[0],
      datenow = document.getElementsByClassName('time')[0];


async function thewather(){
    loding.innerText = '加载中';
    const now = await new Promise((reslove,rejeck)=>{
        const xhr = new XMLHttpRequest();
        xhr.open('GET','https://devapi.qweather.com/v7/weather/now?key=fe86a9f3dc02405d9348157ad298bcfe&location=101120101');
        xhr.send();
        xhr.onerror = ()=> innerText = '网络出现了问题';
        xhr.responseType = 'json';
        xhr.onreadystatechange = function(){
            if(xhr.readyState === 4){
                if(xhr.status >= 200 && xhr.status <300){
                    if(xhr.response.code >= 300){
                        rejeck('此链接失效请联系作者');
                    }else{
                        reslove(xhr.response.now)
                    }
                }else{
                    rejeck('连接有错请联系作者');
                }
            }
        }
    });
    return now;
}
thewather().then(now =>{
    loding.innerText = '加载完成';
    wendu.innerText =  now.temp + '度' ;
    tianqi.innerText = now.text;
    yaqiang.innerText = now.pressure;
    yunliang.innerText = now.cloud;
    xiangduiwen.innerText = now.humidity;
    fengxiang.innerText = now.windDir;
    guance.innerText = now.obsTime;
    guance.style.fontSize = '5px';
    fengsu.innerText = now.windSpeed ;
    fengli.innerText = now.windScale;
    nengjian .innerText = now.vis;
    jiangshui.innerText = now.precip;

}).catch(name => {
    loding.innerText = name;
})
let leftch = left.children,rightch = right.children;
buttonleft.addEventListener('click',function(){
    left.style.left = 0;
    right.style.left = '300px';
})
buttonright.addEventListener('click',function(){
    left.style.left = '-300px'
    right.style.left = 0;
})
setInterval(()=>{
    datenow.innerText = (()=>{
        let hours = new Date().getHours(),
            min = new Date().getMinutes(),
            sec = new Date().getSeconds(),
            timenows = [hours,min,sec].map(time =>{
                if(time < 10){
                    return '0' + time  ;
                }else{
                    return time;
                }
            });
            
            return timenows[0] + ':' + timenows[1] + ':' + timenows[2]
    })();
},200)